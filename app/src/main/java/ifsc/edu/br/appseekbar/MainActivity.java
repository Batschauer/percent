package ifsc.edu.br.appseekbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
    SeekBar seekBar;
    TextView textView, tvSalario, tvTaxa;
    EditText etValor;
    NumberFormat formatacaoPercentual = NumberFormat.getPercentInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekBar =(SeekBar) findViewById(R.id.seekbar);
        textView = (TextView) findViewById(R.id.text);
        etValor = (EditText) findViewById(R.id.etInicial);
        tvTaxa = (TextView) findViewById(R.id.text2);
        tvSalario = (TextView) findViewById(R.id.text3);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

    }

    private final SeekBar.OnSeekBarChangeListener seekBarChangeListener=  new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            textView.setText(Integer.toString(seekBar.getProgress()));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };


    public  void sincronizaTextView(){
        int valor= seekBar.getProgress();
        this.textView.setText(formatacaoPercentual.format(valor/100));
    }

    public void calcula(View view){
        int valor = seekBar.getProgress();
        System.out.print("O VALOR E:" + valor);
        double cthun = Double.parseDouble(String.valueOf(etValor.getText()));
        double ragnaros = (cthun)*(valor/100);
        double ragthun =  cthun + ragnaros;

        tvTaxa.setText(Double.toString(ragnaros));
        tvSalario.setText(Double.toString(ragthun));
    }
}

